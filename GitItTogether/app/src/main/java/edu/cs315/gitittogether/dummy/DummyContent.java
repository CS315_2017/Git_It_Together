package edu.cs315.gitittogether.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.  This does not imply that the items themselves are dummies...
     */
    public static final List<StudentItem> ITEMS = new ArrayList<StudentItem>();

    public static ArrayList<String> namesList = new ArrayList<String>() {{
        // simple, stupid way to get names added right here in some static code
        // NOT recommended for real-life, but okay for simple coding demos
        // NEVER do this in your code!
        add("Korbin Ancell");
        add("Darius Bell");
        add("Tiffanie Birrell");
        add("Kolton Burkhalter");
        add("George Bush");
        add("Travis Cook");
        add("Nathan Daniels");
        add("Christopher Gray");
        add("Gideon Luck");
        add("Nnamdi Massally");
        add("Dakota Mathews");
        add("Isaiah Moore");
        add("Alani Peters");
        add("Virginia Pettit");
        add("Isaak Ramirez");
        add("Logan Rollf");
        add("<NULL POINTER>");
        add("Matthew Smetzer");
        add("Connor Smith");
        add("Sebastian Snyder");
        add("Rich Tanner");
        add("Charlie Velazquez");
        add("Nattapat White");
        add("Sam Willis");
        add("John Wolfe");
    }};

    public static ArrayList<String> teamDeets = new ArrayList<String>() {{
        // simple, stupid way to get names added right here in some static code
        // NOT recommended for real-life, but okay for simple coding demos
        add("4");
        add("1");
        add("2");
        add("4");
        add("2");
        add("1");
        add("5");
        add("2");
        add("5");
        add("3");
        add("3");
        add("1");
        add("5");
        add("4");
        add("4");
        add("4");
        add("33");
        add("5");
        add("2");
        add("1");
        add("999");
        add("3");
        add("1");
        add("2");
        add("5");
    }};

    /**
     * A map of sample (dummy) items, by ID.  This does not imply that the items themselves are dummies...
     */
    public static final Map<String, StudentItem> ITEM_MAP = new HashMap<String, StudentItem>();

    private static final int COUNT = 25;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(StudentItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static StudentItem createDummyItem(int position) {
        return new StudentItem(String.valueOf(position), "Student " + position + ": ", grabName(position -1), makeDetails(position), Integer.parseInt(teamDeets.get(position - 1)));
    }

    private static String grabName (int position) {
        return namesList.get(position).toString();
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Student: ").append(position);

        // new crazy stuff to add team affiliation
        // terrible, bad code
        // tell me a better way...
        // I KNOW you know better ways
        String teamNumber = teamDeets.get(position -1);
        if (teamNumber == "1") {
            builder.append("\nTeam Mint");
        } else if (teamNumber == "2") {
            builder.append("\nTeam Red Velvet");
        } else if (teamNumber == "3") {
            builder.append("\nTeam Peanut Butter");
        } else if (teamNumber == "4") {
            builder.append("\nTeam Birthday Cake");
        } else if (teamNumber == "5") {
            builder.append("\nTeam Lemon");
        } else if (teamNumber == "33") {
            builder.append("\nDropped Course");
        } else {
            builder.append("\nTeam Cookie Monster");
        }

        // this silly stubbed loop adds as many "More Details" as the index number of the item!
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class StudentItem {
        public final String id;
        public final String content;
        public final String name;
        public final String details;
        public final int teamNum;

        public StudentItem(String id, String content, String name, String details, int teamNum) {
            this.id = id;
            this.content = content;
            this.name = name;
            this.details = details;
            this.teamNum = teamNum;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
