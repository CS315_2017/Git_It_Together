package edu.cs315.gitittogether.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.StrictMode;

import java.util.UUID;

/**
 * Created by rwt07a on 10/6/2017.
 */

public class StudentObjectModel implements Parcelable {
    private String studentID;
    private String studentName;
    private String studentDetails;
    private int teamNumber;

    public StudentObjectModel() {

    }

    public StudentObjectModel(String studentID, String studentName, String studentDetails, int teamNumber) {

        this.studentID = studentID;
        this.studentName = studentName;
        this.studentDetails = studentDetails;
        this.teamNumber = teamNumber;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentDetails() {
        return studentDetails;
    }

    public void setStudentDetails(String studentDetails) {
        this.studentDetails = studentDetails;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public void setTeamNumber(int teamNumber) {
        this.teamNumber = teamNumber;
    }

    @Override
    public String toString() {
        return "StudentObjectModel{" +
                "studentID='" + studentID + '\'' +
                ", studentName='" + studentName + '\'' +
                ", studentDetails='" + studentDetails + '\'' +
                ", teamNumber=" + teamNumber +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.studentID);
        dest.writeString(this.studentName);
        dest.writeString(this.studentDetails);
        dest.writeInt(this.teamNumber);
    }

    protected StudentObjectModel(Parcel in) {
        this.studentID = in.readString();
        this.studentName = in.readString();
        this.studentDetails = in.readString();
        this.teamNumber = in.readInt();
    }

    public static final Parcelable.Creator<StudentObjectModel> CREATOR = new Parcelable.Creator<StudentObjectModel>() {
        @Override
        public StudentObjectModel createFromParcel(Parcel source) {
            return new StudentObjectModel(source);
        }

        @Override
        public StudentObjectModel[] newArray(int size) {
            return new StudentObjectModel[size];
        }
    };

}
