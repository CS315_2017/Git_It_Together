package edu.cs315.gitittogether.database;

/**
 * Created by rwt07a on 10/6/2017.
 */


// Here we create the SQL statements and strings we will need later to interface with the actual database
    // The SQL_CREATE string is not only our data constructor, but defines our table and the model we will extract from it
    // The fields below reflect the silliness we had in DummyContent earlier

public class StudentTable {

    public static final String TABLE_STUDENTS = "mobileStudents";
    public static final String COLUMN_ID = "studentId"; // a simple hidden key
    public static final String COLUMN_NAME = "studentName";
    public static final String COLUMN_DETAILS = "details";
    public static final String COLUMN_TEAM_NUMBER = "teamNumber";

    public static final String SQL_CREATE =
            "CREATE TABLE " + TABLE_STUDENTS + "(" +
                    COLUMN_ID + " TEXT PRIMARY KEY," +
                    COLUMN_NAME + " TEXT," +
                    COLUMN_DETAILS + " TEXT," +
                    COLUMN_TEAM_NUMBER + " INTEGER" + ");";

    public static final String SQL_DELETE =
            "DROP TABLE " + TABLE_STUDENTS;
}
